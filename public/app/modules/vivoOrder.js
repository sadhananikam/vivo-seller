var loginapp = angular.module("vivoOrder", ['vivoCommon']);

loginapp.controller("orderCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","$window","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, $window, API_URL) {
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[0];
    urlslug = urlslug.replace('.html','');
    $scope.id = urlslug.split('-').slice(1);
    
    $("#seturl").hide();
    
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getOrderById',
        params : {id:$scope.id}
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = '';
        if(response.data !== 'null')
        {
            $scope.resultList = response.data;
            try {
                $scope.products = JSON.parse(response.data.order.items);
            } catch (e) {
                $scope.products = '';
            }
            
            console.log($scope.products);
        }
        else
        {
            console.log('error');
        }
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $scope.generateInvoice = function(){
        waitingDialog.show();
        $http({
            method: 'GET',
            url : API_URL + 'generateInvoice',
            params : {id:$scope.id}
        }).then(function successCallback(response){
            waitingDialog.hide();
            if(response.data.status ==="Success")
            {
                alert("invoice generated");
                $("#seturl").hide();
                var params = response.data.filename.split('/');
                var params_count = params.length;
                var doc = document.getElementById("seturl");
                doc.href = "http://localhost:8000/invoice/" + params[params_count-1];
                doc.download = params[params_count-1];
                
                //Dispatching click event.
                if (document.createEvent) {
                    var e = document.createEvent('MouseEvents');
                    e.initEvent('click', true, true);
                    doc.dispatchEvent(e);
                    return true;
                }
            }
            else if(response.data.status ==="Error")
            {
                alert("Error while generating invoice, try again");
            }
        },function errorCallback(response){
            console.log(response.data);
        });
    };
}]);