<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return \File::get(public_path() . '/partials/login.html');
});

Route::get('/dashboard', function () {
    return \File::get(public_path() . '/partials/dashboard.html');
});

Route::get('/customers', function () {
    return \File::get(public_path() . '/partials/customer-list.html');
});

Route::get('/products', function () {
    return \File::get(public_path() . '/partials/product-list.html');
});

Route::get('/orders', function () {
    return \File::get(public_path() . '/partials/order-list.html');
});

Route::get('/customer-{id}', function ($id) {
    return \File::get(public_path() . '/partials/customer.html');
});

Route::get('/product-{id}', function ($id) {
    return \File::get(public_path() . '/partials/product.html');
});

Route::get('/order-{id}', function ($id) {
    return \File::get(public_path() . '/partials/order.html');
});

Route::get('/user-profile', function () {
    return \File::get(public_path() . '/partials/user-profile.html');
});

Route::get('/api/v1/session','UserController@session');
Route::post('/api/v1/login','UserController@login');
Route::get('/api/v1/logout','UserController@logout');
Route::get('/api/v1/getAllCustomers','UserController@getAllCustomers');
Route::get('/api/v1/getAllProducts','UserController@getAllProducts');
Route::get('/api/v1/getAllOrders','UserController@getAllOrders');
Route::get('/api/v1/getCustomerById','UserController@getCustomerById');
Route::get('/api/v1/getProductById','UserController@getProductById');
Route::get('/api/v1/getOrderById','UserController@getOrderById');
Route::get('/api/v1/getUserProfile','UserController@getUserProfile');
Route::get('/api/v1/getDashboardStats','UserController@getDashboardStats');
Route::get('/api/v1/generateInvoice',array('as'=>'generateInvoice','uses'=>'UserController@generateInvoice'));

/*Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
