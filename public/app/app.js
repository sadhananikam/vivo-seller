var app = angular.module("vivo", ['ui.router', 'ngSanitize', 'ui.select', 'ngAnimate', 'toaster', 'kendo.directives', 'satellizer', 'tagged.directives.infiniteScroll']);

function logoutJquery() {
    $.get("api/v1/int-logout", function (data) {

        location.reload();

    });
}

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m" style="width: 300px;">' +
        '<div class="modal-content"style="background-color: rgba(255, 0, 0, 0);box-shadow: none;border: none;">' +

        '<div class="modal-body text-center">' +
        '<svg width="60px" height="60px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="47.5" stroke-dasharray="193.99334635916975 104.45795573186061" stroke="#e62739 " fill="none" stroke-width="5"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>' +
        '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Please wait';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);


app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

$urlRouterProvider.otherwise("i/internal/login");
    $stateProvider

        .state('i/internal/internalDetail', {
            url: '/i/internal/internalDetail',
            controller: 'internalOrderCtrl',
            templateUrl: 'partials/internalDetail.html'
        })
        .state('i/internal/coupon', {
            url: '/i/internal/coupon',
            controller: 'couponCtrl',
            templateUrl: 'partials/coupon.html'
        })
        .state('i/internal/home', {
            url: '/i/internal/home',
            controller: 'homeCtrl',
            templateUrl: 'partials/home.html'
        })
        .state('i/internal/login', {
            url: '/i/internal/login',
            controller: 'loginCtrl',
            templateUrl: 'partials/login.html'
        })
        .state('i/internal/banner', {
            url: '/i/internal/banner',
            controller: 'bannerCtrl',
            templateUrl: 'partials/banner.html'
        })
        .state('i/internal/lookbook', {
            url: '/i/internal/lookbook',
            controller: 'lookbookEditCtrl',
            templateUrl: 'partials/lookbookEdit.html'
        })
        .state('i/internal/post', {
            url: '/i/internal/post',
            controller: 'postCtrl',
            templateUrl: 'partials/post.html'
        })
        .state('i/internal/product', {
            url: '/i/internal/product',
            controller: 'productCtrl',
            templateUrl: 'partials/product.html'
        })


        }])
    
.run(["$rootScope", "Data", "$location", function ($rootScope, Data, $location) {

    $rootScope.login = {};
    $rootScope.customer = {
        name: null,
        password: null
    }
    
    $rootScope.$on("$stateChangeStart", function (event, next, current) {
        $rootScope.authenticated = false;
        Data.get('int-session').then(function (results) {
            if (results.id) {
                $rootScope.authenticated = true;
                $rootScope.user = results;
                var nextUrl = next.url;
                if (nextUrl == '/i/internal/login') {
                    $location.path("i/internal/internalDetail");
                }
            } else {
                var nextUrl = next.url;
                if (nextUrl == '/i/internal/internalDetail' || nextUrl == '/i/internal/coupon' || nextUrl == '/i/internal/home' || nextUrl == '/i/internal/banner' || nextUrl == '/i/internal/lookbook' || nextUrl == '/i/internal/product' || nextUrl == '/i/internal/post') {
                    $location.path("i/internal/login");
                }
            }
        });
    });
    
    $rootScope.logout = function () {
        Data.get('logout').then(function (results) {
            Data.toast(results);
            $window.location.reload();
        });
    }

    $rootScope.doLogin = function (customer) {
        waitingDialog.show(); 
        if ((customer.name == "" || customer.name == null) && (customer.password == "" || customer.password == null)) {
            waitingDialog.hide(); 
            $rootScope.response = "Username and password is required";
        }
        else if (customer.name == "" || customer.name == null) {
            waitingDialog.hide(); 
            $rootScope.response = "Username is required";
        }
        else if(customer.password == "" || customer.password == null)
        {
            waitingDialog.hide(); 
            $rootScope.response = "Password is required";
        }
        else 
        {
            Data.post('int-login', {
                customer: customer
            }).then(function (results) {
                waitingDialog.hide(); 
                $rootScope.user = results;
                $rootScope.response = results.message;
//                alert(results.message);
                if (results.status == "success") {
                    $rootScope.isAuthenticated = true;
                    $rootScope.goToHome();
                }
            });
        }
    }

}]);

app.controller("internalOrderCtrl", ["$scope", "$http", function ($scope, $http) { 
    $scope.q = null;
    $scope.getDetail = function () {
        
        if ($scope.type == 'orderid') {
            $http.get('api/v1/getInternalDetailByOrderId.php?q=' + $scope.q).success(function (data) {
                $scope.tempList = [];
                for (var i = 0; i < data.length; i++) {
                    $scope.tempList[i] = data[i];
                    $scope.tempList[i].items = JSON.parse(data[i].items);
                }
                $scope.resultList = $scope.tempList;
            }).error(function (data) {

            });
        }
        
        if ($scope.type == 'uid') {
            $http.get('api/v1/getInternalDetailByUserId.php?q=' + $scope.q).success(function (data) {
                $scope.tempList = [];
                for (var i = 0; i < data.length; i++) {
                    $scope.tempList[i] = data[i];
                    $scope.tempList[i].items = JSON.parse(data[i].items);
                }
                $scope.resultList = $scope.tempList;
            }).error(function (data) {

            });
        }

        if ($scope.type == 'emailid') {
            $http.get('api/v1/getInternalDetailByEmailId.php?q=' + $scope.q).success(function (data) {
                $scope.tempList = [];
                for (var i = 0; i < data.length; i++) {
                    $scope.tempList[i] = data[i];
                    $scope.tempList[i].items = JSON.parse(data[i].items);
                }
                $scope.resultList = $scope.tempList;
            }).error(function (data) {

            });
        }

    }

    $scope.status = null;

    $scope.getAddress = function (id) {
        $http.get('api/v1/getAddressById.php?id=' + id).success(function (data) {
            $scope.add = data[0];
            $("#address").modal('show');
        }).error(function (data) {

        });

    }
    
    $scope.setFinalStatus = function (status) {
        if ($scope.type == 'orderid') {
            $http.get('api/v1/setStatus.php?status=' + status + '&oid=' + $scope.selectedOrder.id + '&name=' + $scope.selectedOrder.name + '&email=' + $scope.selectedOrder.email + '&d=' + $scope.confirmDate).success(function (data) {
                $scope.getDetail();
            }).error(function (data) {

            });
        }
        
        if ($scope.type == 'uid') {
            $http.get('api/v1/setStatus.php?status=' + status + '&oid=' + $scope.selectedOrder.id + '&name=' + $scope.selectedOrder.name + '&email=' + $scope.selectedOrder.email + '&d=' + $scope.confirmDate).success(function (data) {
                $scope.getDetail();
            }).error(function (data) {

            });
        }

        if ($scope.type == 'emailid') {
            $http.get('api/v1/setStatus.php?status=' + status + '&oid=' + $scope.selectedOrder.id + '&name=' + $scope.selectedOrder.name + '&email=' + $scope.selectedOrder.email + '&d=' + $scope.confirmDate).success(function (data) {
                $scope.getDetail();
            }).error(function (data) {

            });

        }

    }
    
    $scope.setStatus = function (status, o) {
        if (status == 'CONFIRMED' || status == 'PROCESSING' || status=='DISPATCHED'  || status=='CANCELLED') {
            $scope.confirmDate = null;
            $scope.showDate = true;

        } else {
            $scope.showDate = false;
        }
        
        $("#confirm").modal('show');
        $scope.newStatus = status;
        $scope.selectedOrder = o;
    }
    
    $('.getDetail').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.getDetail();
        }
    });

}]);

app.controller("couponCtrl", ["$scope", "$http", "$rootScope", "$location", function ($scope, $http, $rootScope, $location) {
    $scope.addCoupon = function () {
    $http.get('api/v1/addCoupon.php?coupon_code=' + $scope.coupon_code + '&coupon_value=' + $scope.coupon_value + '&description=' + $scope.description + '&min_amount=' + $scope.min_amount + '&status=' + $scope.status).success(function (data) {
            $scope.getCoupon();
        
            $("#addcoupon").modal('show');

            $timeout(function () {
                $("#addcoupon").modal('hide');
            }, 2000);
            
            $scope.a = {
                coupon_code: null,
                coupon_value: null,
                description: null,
                min_amount:null,
                status: null
            }
        
        }).error(function (data) {
            console.log(data);
        });

    }
    
    $scope.setStatus = function (id, status) {
    $http.get('api/v1/updateCouponStatus.php?id=' + id + '&s=' + status).success(function (data) {
            $scope.getCoupon();
        }).error(function (data) {
            console.log(data);
        });
    }
    
    $scope.getCoupon = function () {
    $http.get('api/v1/getCoupons.php').success(function (data) {
        $scope.coupons = data;
            console.log(data);
            $location.path("i/internal/coupon");
        }).error(function (data) {
            console.log(data);
        });
    }
    
    $scope.isEdit = false;
    $scope.editCoupon = function (o) {
        $scope.isEdit = true;
        $http.post('api/coupon/updateCoupon.php', o).success(function (data) {
            $scope.coupons = data;
            $scope.getCoupon();
            $scope.isEdit = false;

        }).error(function (data) {

        });
    }
    
    $scope.deleteCoupon = function (id) {
        $http.post('api/coupon/deleteCoupon.php',{id:id}).success(function (data) {   $scope.coupons = data;
            $scope.getCoupon();

            $("#deletecoupon").modal('show');

            $timeout(function () {
                $("#deletecoupon").modal('hide');
            }, 2000);
                                                                                   
        }).error(function (data) {

        });
    }
    
    $scope.getCoupon();

    }]);

app.controller("bannerCtrl", ["$scope", "$http", "$location","$timeout", function ($scope, $http, $location,$timeout) {
    $scope.Banner = function () {
        $("html,body").animate({
            scrollTop: $('#addBanner').position().top
        }, 800);
    }
    
    $scope.addBanner = function () {
        $http.get('api/v1/addBanner.php?b_url=' + $scope.b_url + '&b_alt=' + $scope.b_alt + '&b_page=' + $scope.b_page + '&b_target=' + $scope.b_target + '&b_href=' + $scope.b_href).success(function (data) {
            $scope.getBanner();
            
            $("#addbanner").modal('show');

            $timeout(function () {
                $("#addbanner").modal('hide');
            }, 2000);
            
        $scope.a = {
        b_url: null,
        b_alt: null,
        b_page: null,
        b_target:null,
        b_href: null
    }

        }).error(function (data) {
            console.log(data);
        });

    }


    $scope.setPriority = function (id, sequence) {
        $http.get('api/v1/updateBannerSequence.php?id=' + id + '&a=' + sequence).success(function (data) {
            $scope.getBanner();
        }).error(function (data) {
            console.log(data);
        });

    }

    $scope.setActive = function (id, is_active) {
        is_active == true ? is_active = 1 : is_active = 0;
        $http.get('api/v1/updateBannerStatus.php?id=' + id + '&a=' + is_active).success(function (data) {
            $scope.getBanner();
        }).error(function (data) {
            console.log(data);
        });

    }

    $scope.setMobile = function (id, is_Mobile) {
        is_Mobile == true ? is_Mobile = 1 : is_Mobile = 0;
        $http.get('api/v1/updateBannerMobileStatus.php?id=' + id + '&is_Mobile=' + is_Mobile).success(function (data) {
            $scope.getBanner();
        }).error(function (data) {
            console.log(data);
        });

    }
    
    $scope.isEdit = false;
    $scope.editBanner = function (o) {
        $scope.isEdit = true;
        $http.post('api/banner/updateBanner.php', o).success(function (data) {
            $scope.banner = data;
            $scope.getBanner();
            $scope.isEdit = false;

        }).error(function (data) {

        });
    }
    
    $scope.deleteBanner = function (id) {
        $http.post('api/banner/deleteBanner.php',{id:id}).success(function (data) {   $scope.banner = data;
            $scope.getBanner();
                                                                                   
            $("#deletebanner").modal('show');

            $timeout(function () {
                $("#deletebanner").modal('hide');
            }, 2000);

        }).error(function (data) {

        });
    }

    $scope.setLink = function (id, is_link) {
        is_link == true ? is_link = 1 : is_link = 0;
        $http.get('api/v1/updateBannerLinkStatus.php?id=' + id + '&is_link=' + is_link).success(function (data) {
            $scope.getBanner();
        }).error(function (data) {
            console.log(data);
        });

    }

    $scope.getBanner = function () {
        $http.get('api/v1/getBanner.php').success(function (data) {
            $scope.banner = data;
            console.log(data);
            $location.path("i/internal/banner");
        }).error(function (data) {
            console.log(data);
        });

    }

    $scope.getBanner();


    }]);

app.controller("lookbookEditCtrl", ["$scope", "$http", "$stateParams","$timeout", function ($scope, $http, $stateParams, $timeout) {

    $scope.Blog = function () {
        $("html,body").animate({
            scrollTop: $('#addBlog').position().top
        }, 800);
    }
    
    $http.get("api/blog/getBlogDetail.php?id=" + $stateParams.id).success(function (data) {
        $scope.products = data;
        $scope.blog = data[0];
        $scope.getRelatedBlog();
        if (data == "invalid") {
            $("#urlinvalid").modal('show');

            $timeout(function () {
                $("#urlinvalid").modal('hide');
            }, 2000);

        } else {
            var result = data;
        }

    }).error(function (data) {
        console.log(data);
    });

    $scope.getBlogs = function () {
        $http.get('api/blog/getTitle.php').success(function (data) {
            $scope.blogs = data;

        }).error(function (data) {
            console.log(data);
        });
    }

    $scope.getBlogs();
    $scope.getRelatedBlog = function () {
        $http.get("api/blog/getRelatedBlog.php?id1=" + $scope.blog.look_1 + "&id2=" + $scope.blog.look_2 + "&id3=" + $scope.blog.look_3).success(function (data) {
            $scope.related = data;
        }).error(function (data) {
            console.log(data);
        });

    }

    $scope.a = {
        title: null,
        lb_short_description: null,
        lb_long_description: null,
        like_count: null,
        banner_img: null,
        similar_1: null,
        similar_2: null,
        similar_3: null,
        similar_4: null,
        browse_link: null,
        look_1: null,
        look_2: null,
        look_3: null
    }
    
    $scope.isEdit = false;
    $scope.editBlog = function (o) {
        $scope.isEdit = true;
        $http.post('api/blog/updateBlog.php', o).success(function (data) {
            $scope.blog = data;
            $scope.getBlogs();
            $scope.isEdit = false;
            
            $("#updateblog").modal('show');

            $timeout(function () {
                $("#updateblog").modal('hide');
            }, 2000);

        }).error(function (data) {

        });
    }


    $scope.addBlog = function () {
        $http.post('api/blog/addBlog.php', $scope.a).success(function (data) {
            $scope.blog = data;
            $scope.getBlogs();
            
            $("#addblog").modal('show');

            $timeout(function () {
                $("#addblog").modal('hide');
            }, 2000);
            
        $scope.a = {
        title: null,
        lb_short_description: null,
        lb_long_description: null,
        category:null,
        banner_img: null,
        similar_1: null,
        similar_2: null,
        similar_3: null,
        similar_4: null,
        browse_link: null,
        look_1: null,
        look_2: null,
        look_3: null,
        tag1:null,
        tag2:null,
        tag3:null,
        look_1:null,
        look_2:null,
        look_3:null
    }

        }).error(function (data) {

        });
    }
    
    $scope.deleteBlog = function (id) {
        $http.post('api/blog/deleteBlog.php',{id:id}).success(function (data) {
            $scope.blog = data;
            $scope.getBlogs();
            
            $("#deleteblog").modal('show');

            $timeout(function () {
                $("#deleteblog").modal('hide');
            }, 2000);

        }).error(function (data) {

        });
    }
    
    $scope.setCategory = function (id, category) {

        $http.get('api/blog/setCategory.php?id=' + id + '&c=' + category).success(function (data) {
            $scope.getBlogs();
        }).error(function (data) {
            console.log(data);
        });

    }


    }]);

app.controller("loginCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location", function ($scope, $http, $stateParams, Data, $rootScope, $location) {
    
    $rootScope.goToHome = function () {
        $location.path('i/internal/internalDetail');
    }

    $(document).bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $rootScope.goToHome();
            $scope.$apply();
        }
    });

    }]);

app.controller("homeCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope","$location","$timeout", function ($scope, $http, $stateParams, Data, $rootScope,$location,$timeout) {

    $rootScope.logout = function () {
        Data.get('logout').then(function (results) {
            Data.toast(results);
            $window.location.reload();
        });
    }
    }]);

app.controller("postCtrl", ["$scope", "$http", "$rootScope", "$location", function ($scope, $http, $rootScope, $location) {
 
$scope.isEdit = false;
    $scope.editCoupon = function (o) {
        $scope.isEdit = true;
        $http.post('api/post/updatePost.php', o).success(function (data) {
            $scope.posts = data;
            $scope.getPost();
            $scope.isEdit = false;

        }).error(function (data) {

        });
    }

    }]);

app.controller("productCtrl", ["$scope", "$http", "$rootScope", "$location","$timeout", function ($scope, $http, $rootScope, $location,$timeout) {
    //$('#noproduct').hide();
    $scope.parent_category= null;
    $scope.category= null;
    $scope.jeweller= null;
    $scope.start_price= null;
    $scope.last_price= null;
    $scope.metal= null;
    $scope.gemstone= null;
    $scope.diamond_count= null;
    
    $scope.toggleGuess=function(){
        if($scope.jeweller=='') 
            $('.suggestjeweller').hide();
        else 
            $('.suggestjeweller').show();
    }
    
    $( "#jeweller" ).focusout(function() 
    {
        $('.suggestjeweller').hide();
    });
    
    $scope.getProductsForSort = function () 
    {    
        if($scope.parent_category == null || $scope.parent_category == '' || $scope.category== null || $scope.category== '')
        {
            $("#formError").modal('show');

            $timeout(function () {
                $("#formError").modal('hide');
            }, 2000);
        }
        else
        {
            $scope.resultList = '';
            $http.get('api/v1/getProductsForSort.php?parent_category=' + $scope.parent_category + '&category=' + $scope.category + '&jeweller=' + $scope.jeweller + '&start_price=' + $scope.start_price + '&last_price=' + $scope.last_price + '&metal=' + $scope.metal + '&gemstone=' + $scope.gemstone + '&diamond_count=' + $scope.diamond_count).success(function (data) 
                {   
                    console.log(data);
                    //alert(data.length);
                    if(data.length>=1)
                    {
                        $scope.resultList = data;
                        $scope.displaygrid();
                    }
                    else
                    {
                        $scope.resultList = '';
                    }

                }).error(function (data) {
                    console.log(data);
                });
        }

    }
    
    $scope.saveProduct = function(id)
    {
        var sortorder = $('#'+id+'-sort').val();
        var primary = $('#'+id+'-prim').val();
        var secondary = $('#'+id+'-sec').val();
        //alert(id + "," + sortorder);
        
        if(sortorder.length)
        {
            $http.get('api/v1/updateProductSortorder.php?id=' + id + '&sortorder=' + sortorder + '&primary=' + primary + '&secondary=' + secondary).success(function (data) {
                console.log(data);
            	if(data == "success")
            	{
                    setTimeout(function(){
                      $scope.getProductsForSort();
                    }, 1000);
            	}
            	else
            	{
            		alert('No changes in database');
            	}
            }).error(function (data) {
                console.log(data);
            });
        }        
    }
    
    $scope.displaygrid = function(){
        $scope.mainGridOptions = {
            dataSource:{
                data : $scope.resultList,
                //total: $scope.resultList.length,
                pageSize: 5,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "id",
                title: "Id",
                width: 50
            },
            {
                template: "<div class='product-photo'" +
                                "style='background-image: url(https://www.vivocarat.com/images/products-v2/#: VC_SKU #-1.jpg);'></div>",
                field: "title",
                title: "Image",
                width: 50
            }, {
                field: "VC_SKU",
                title: "VC_SKU",
                width: 80
            },{
                field: "stone_pieces",
                title: "DIAMOND COUNT",
                width: 50
            },{
                field: "min_total_diamond_weight",
                title: "MIN DIAMOND WT",
                width: 80
            }, {
                field: "price_after_discount",
                title: "Price",
                width: 50
            },{
                field: "sortorder",
                title: "Sort Order",
                template: "<input id='#:id#-sort' type='number' data-bind='value:sortorder' min='0' value='#:sortorder#' style='width: 50px;'/>",
                width: 60
            },{
                field: "primary_tags",
                title: "Primary Tags",
                template: "<textarea id='#:id#-prim'>#:primary_tags#</textarea>",
                width: 110
            },{
                field: "secondary_tags",
                title: "Secondary Tags",
                template: "<textarea id='#:id#-sec'>#:secondary_tags#</textarea>",
                width: 110
            },{ 
                template: "<a class='btn save-product' data-ng-click='saveProduct(#:id#)'>SAVE</a>", 
                title: "", 
                width: "70px" 
            }

            ]
        }
    }
    
    $scope.getAllJeweller = function () {
    $http.get('api/v1/getAllJeweller.php').success(function (data) 
        {
            $scope.jeweller = '';
            $scope.jnames = data;
        
            console.log($scope.jnames);
        
        }).error(function (data) {
            console.log(data);
        });

    }
    $scope.getAllJeweller();
    
    
}]);

app.directive('vivoHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
//Sticky header 
            $scope.logo = false;

            $(window).bind('scroll', function () {
                //Need to change the height here as well as in ".fixed class" of vivo-header file to make it work //properly
                var navHeight = 50;
                if ($(window).scrollTop() > navHeight) {
                    $('nav').addClass('fixed');
                    $scope.logo = true;
                    $scope.$apply();

                } else {
                    $('nav').removeClass('fixed');
                    $scope.logo = false;
                    $scope.$apply();
                }
            });

            //END of Sticky header

        },
        link: function ($scope, $rootScope, Data, $location) {

        },
        
        templateUrl: 'partials/vivo-header.html'
    };
});

app.directive('vivoFooter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {




        },
        link: function ($scope, $rootScope, Data, $location) {



        },
        templateUrl: 'partials/vivo-footer.html'
    };
});