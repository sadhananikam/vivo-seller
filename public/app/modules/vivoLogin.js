var loginapp = angular.module("vivoLogin", ['vivoCommon']);

loginapp.controller("loginCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
       
    $rootScope.goToHome = function () {
        window.location.href = "/dashboard";
    }

    $(document).bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $rootScope.goToHome();
            $scope.$apply();
        }
    });
}]);