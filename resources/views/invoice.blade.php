<style>
    /** { padding: 0; margin: 0; font-family: Arial,  sans-serif; box-sizing: border-box; font-size: 14px; line-height: 1.75; }*/
/*            body { padding: 50px; width: 8.27in; height: 11.69in; margin: auto; border: 1px solid #ccc; }*/
    * { font-family: Arial,  sans-serif; font-size: 14px; }
    header { text-align: center; }
    nav { border-top: 1px solid; border-bottom: 1px solid; margin-top: 15px; }
    section { margin: 15px 0; }
    h1 { text-align: center; font-size: 21px; }
    h2 { font-size: 18px; }
    .col-1, .col-2 { display: inline-block; }
    .col-1 { width:60%; }
    .col-2 { width:40%; }
    table { text-align: center; }
    table { border-collapse: collapse; }
    td, th { border: 1px solid; }
    table.tbl-exclude { width: 100%; }
    table.tbl-exclude td { border: none; padding: 0; text-align: left; }
    td, th { padding: 8px; }
    .b0 { border-width: 0; }
    .w-60pc { width: 60%; }
    .regards { text-align: right; }
    .text-center { text-align: center; }
    .t10 { font-size: 10px; }
</style>

<div class="wrapper">
    <header>
        <img class="logo" src="https://www.vivocarat.com/images/header/logos/VivoCarat%20logo.png" alt="vivocarat logo" />
        <nav>Phone: +91 9800000000 | Email: hello@vivocarat.com</nav>
    </header>
    <container>
        <h1>Retail/Tax Invoice</h1>
        <section>
            <table class="tbl-exclude">
                <tr>
                    <td class="w-60pc">
                        <h2>Supplied by:</h2>
                        <p>
                            M/s Somebody,<br>
                            Somewhere, <br>
                            Near Something, <br>
                            A Station (w)
                        </p>
                    </td>
                    <td>
                        <h2>Order Details:</h2>
                        <table>
                            <tr>
                                <td class="w-60pc">
                                    Order ID:
                                </td>
                                <td>
                                    {{$order_data->id}}
                                </td>
                            </tr>
                            <tr>
                                <td class="w-60pc">
                                    Date:
                                </td>
                                <td>
                                    {{$order_data->placed_on}}
                                </td>
                            </tr>
                            <tr>
                                <td class="w-60pc">
                                    Payment Mode:
                                </td>
                                <td>
                                    {{$order_data->payment_mode}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </section>
        <section>
            <table class="tbl-exclude">
                <tr>
                    <td class="w-60pc">
                        <h2>Billing Address:</h2>
                        <p>
                            {{$bill_address->name}}<br>
                            {{$bill_address->first_line}}<br>
                            {{$bill_address->city}} - {{$bill_address->pin}}<br>
                            {{$bill_address->state}}<br>
                            Phone:{{$bill_address->phone}}<br>
                            Email:{{$user->email}}
                        </p>
                    </td>
                    <td>
                        <h2>Shipping Address:</h2>
                        <p>
                            {{$ship_addres->name}}<br>
                            {{$ship_addres->first_line}}<br>
                            {{$ship_addres->city}} - {{$ship_addres->pin}}<br>
                            {{$ship_addres->state}}<br>
                            Phone:{{$ship_addres->phone}}<br>
                            Email:{{$user->email}}
                        </p>
                    </td>
                </tr>
            </table>
        </section>
        <section>
            <table>
                <tbody>
                    <tr>
                        <th>Product Description</th>
                        <th>Qty</th>
                        <th>Unit Price</th>
                        <th>Total Price</th>
                        <th>Tax Rate (GST)</th>
                        <th>Total Tax Amount</th>
                        <th>Total Amount</th>
                    </tr>
                    @foreach ($products as $key => $item)
                    <tr>
                        <td>{{ $item['title'] }}</td>
                        <td>{{ $item['quantity'] }}</td>
                        <td>{{ $item['unit_price'] }}</td>
                        <td>{{ $item['total_price'] }}</td>
                        <td>{{ $item['tax_rate'] }}</td>
                        <td>{{ $item['total_tax_amount'] }}</td>
                        <td>{{ $item['total_amount'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="border: none !important;" colspan="4"></td>
                        <td colspan="2">Shippiing</td>
                        <td>Free</td>
                    </tr>
                    <tr>
                        <td style="border: none !important;" colspan="4"></td>
                        <td colspan="2">Grand Total (INR)</td>
                        <td>{{ $grand_total }}</td>
                    </tr>
                </tbody>
            </table>
        </section>
        <section>
            <p class="text-center">Amount in Words: {{$words}}</p>
        </section>
        <section>
           <p class="regards">
                For Lajja Creation
               <br><br><br>
               Authorised Signature
            </p>
        </section>
    </container>
    <footer>
        <p class="text-center t10">The goods sold as part of this shipment are intended purely for personal use & not for any commercial transaction</p>
    </footer>
</div>