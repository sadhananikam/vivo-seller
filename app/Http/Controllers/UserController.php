<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PDF;
use File;

class UserController extends Controller {
    
    // blowfish
    private static $algo = '$2a';
    // cost parameter
    private static $cost = '$10';

    // mainly for internal use
    public static function unique_salt() {
        return substr(sha1(mt_rand()), 0, 22);
    }

    // this will be used to generate a hash
    public static function hash($password_new) {

        return crypt($password_new, self::$algo .
                self::$cost .
                '$' . self::unique_salt());
    }

    // this will be used to compare a password against a hash
    public static function check_password($hash2, $password2) {
        $full_salt = substr($hash2, 0, 29);
        $new_hash = crypt($password2, $full_salt);
        return ($hash2 == $new_hash);
    }
    
    /**
     * Verifying required params posted or not
     */
    public function verifyRequiredParams($required_fields,$request_params) 
    {
        $error = false;
        $error_fields = "";
        foreach ($required_fields as $field) {
            if (!isset($request_params->$field) || strlen(trim($request_params->$field)) <= 0) {
                $error = true;
                $error_fields .= $field . ', ';
            }
        }

        if ($error) {
            // Required field(s) are missing or empty
            // echo error json and stop the app
            $response = array();
            //$app = \Slim\Slim::getInstance();
            $response["status"] = "error";
            $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
            return json_encode($response);
            //echoResponse(200, $response);
            //$app->stop();
        }
    }
    
    // Time format is UNIX timestamp or
    // PHP strtotime compatible strings
      public function dateDiff($time1, $time2, $precision = 6) {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
          $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
          $time2 = strtotime($time2);
        }

        // If time1 is bigger than time2
        // Then swap time1 and time2
        if ($time1 > $time2) {
          $ttime = $time1;
          $time1 = $time2;
          $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year','month','day','hour','minute','second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval) {
          // Create temp time from time1 and interval
          $ttime = strtotime('+1 ' . $interval, $time1);
          // Set initial values
          $add = 1;
          $looped = 0;
          // Loop until temp time is smaller than time2
          while ($time2 >= $ttime) {
            // Create new temp time from time1 and interval
            $add++;
            $ttime = strtotime("+" . $add . " " . $interval, $time1);
            $looped++;
          }

          $time1 = strtotime("+" . $looped . " " . $interval, $time1);
          $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value) {
          // Break if we have needed precission
          if ($count >= $precision) {
            break;
          }
          // Add value and interval 
          // if value is bigger than 0
          if ($value > 0) {
            // Add s if value is not 1
            if ($value != 1) {
              $interval .= "s";
            }
            // Add value and interval to times array
            $times[] = $value . " " . $interval;
            $count++;
          }
        }

        // Return string with times
        return implode(", ", $times);
    }

    public function array_to_object($array) {
        return (object) $array;
    }
    
    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if(isset($_SESSION['id']))
        {
            $sess["id"] = $_SESSION['id'];
            $sess["username"] = $_SESSION['username'];
            $sess["role"] = $_SESSION['role'];
            $sess["seller_id"] = $_SESSION['seller_id'];
        }
        else
        {
            $sess["id"] = '';
            $sess["username"] = 'Guest';
            $sess["role"] = '';
            $sess["seller_id"] = '';
        }
        return $sess;
    }
    
    public function destroySession(){
        if (!isset($_SESSION)) {
        session_start();
        }
        if(isSet($_SESSION['id']))
        {
            unset($_SESSION['id']);
            unset($_SESSION['username']);
            unset($_SESSION['role']);
            unset($_SESSION['seller_id']);
            $info='info';
            if(isSet($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }
            $msg="Logged Out Successfully...";
        }
        else
        {
            $msg = "Not logged in...";
        }
        return $msg;
    }

    public function session(){
        $session = $this->getSession();
        $response["id"] = $session['id'];
        $response["role"] = $session['role'];
        $response["username"] = $session['username'];
        $response["seller_id"] = $session['seller_id'];
        return json_encode($session);
    }
    
    public function logout(){
        
        $session = $this->destroySession();
        $response["status"] = "info";
        $response["message"] = "Logged out successfully";
        return json_encode($response);
    }
    
    public function login()
    {
        $r = json_decode($_REQUEST['customer']);
        
        $response = array();
        
        $password1 = $r->password;
        $username = $r->name;
        
        $userArr = DB::table('dashboard_user')
                    ->where('username',$username)
                    ->first();
        
        if ($userArr != NULL) 
        {
            $user = json_decode(json_encode($userArr), True);
            
            if($user['password']==md5($password1))
            {
                $response['status'] = "success";
                $response['message'] = 'Logged in successfully.';
                
                $response['username'] = $user['username'];
                $response['id'] = $user['id'];
                $response['role'] = $user['role']; 
                $response['seller_id'] = $user['seller_id'];
                $response['createdAt'] = $user['created_at'];
                
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['id'] = $user['id'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['role'] = $user['role'];
                $_SESSION['seller_id'] = $user['seller_id'];
            } 
            else 
            {
                $response['status'] ='error';
                $response['message'] = 'Login failed. Incorrect credentials';
            }
        }
        else 
        {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
        return json_encode($response);
    }
    
    public function getAllCustomers()
    {
        $session = $this->getSession();
        $seller_id = $session['seller_id'];
        
        if($seller_id == 0)
        {
            $userArr = DB::table('customers_auth')
                    ->select('uid As id','name','email','phone','gender','created_at')
                    ->orderBy('uid','desc')
                    ->get();
        }
        else
        {
            $userArr = DB::table('customers_auth')
                    ->select('uid As id','name','email','phone','gender','created_at')
                    ->where('register_through',$seller_id)
                    ->orderBy('uid','desc')
                    ->get();
        }        
        return json_encode($userArr);
    }
    
    public function getAllProducts()
    {
        $session = $this->getSession();
        $seller_id = $session['seller_id'];
        
        if($seller_id == 0)
        {
            $products = DB::table('products')
                        ->orderBy('id','desc')
                        ->groupBy('VC_SKU')
                        ->get();
        }
        else
        {
            $products = DB::table('products')
                        ->where('jeweller_id',$seller_id)
                        ->orderBy('id','desc')
                        ->groupBy('VC_SKU')
                        ->get();
        }
        return json_encode($products);
    }
    
    public function getAllOrders()
    {
        $session = $this->getSession();
        $seller_id = $session['seller_id'];
        
        if($seller_id == 0)
        {
            $orders = DB::table('orders_temp')
                        ->orderBy('id','desc')
                        ->get();
        }
        else
        {
            $orders = DB::table('orders_temp')
                    ->where('visited_through',$seller_id)
                    ->orderBy('id','desc')
                    ->get();
        }
        return json_encode($orders);
    }
    
    public function getCustomerById()
    {
        $uid = $_REQUEST['uid'];
        $arr = array();
        
        $user = DB::table('customers_auth')
                    ->select('uid As id','name','email','phone','gender','dob','anniversary','created_at','register_through','register_url','login_through','login_url','updated_at')
                    ->where('uid',$uid)
                    ->first();
        
        $address = DB::table('customers_address')
                    ->where('uid',$uid)
                    ->first();
        
        $order = DB::table('orders_temp')
                    ->where('uid',$uid)
                    ->get();
        
        $cart = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('is_active',1)
                    ->get();
        
        $wishlist = DB::table('watchlist')
                    ->where('uid',$uid)
                    ->get();
        
        $visit = DB::table('customers_page_visits')
                    ->where('uid',$uid)
                    ->orderBy('id','desc')
                    ->get();
        
        $logtime = $user->updated_at;
        $curtime = date("Y-m-d H:i:s");
        $logdatediff = $this->dateDiff($curtime, $logtime);
        
        $addressdatediff = '';
        
        if($address != null || $address !='')
        {
            $addresstime = $address->updated_at;
            $curtime = date("Y-m-d H:i:s");
            $addressdatediff = $this->dateDiff($curtime, $addresstime);
        }
        
        $arr['customer'] = $user;
        $arr['address'] = $address;
        $arr['order'] = $order;
        $arr['cart'] = $cart;
        $arr['wishlist'] = $wishlist;
        $arr['visit'] = $visit;
        $arr['loggedIn'] = $logdatediff;
        $arr['addressAdd'] = $addressdatediff;
        
        return json_encode($arr);
    }
    
    public function getProductById()
    {
        $id = $_REQUEST['id'];
        $arr = array();
        
        $products = DB::table('products')
                        ->where('id',$id)
                        ->first();
        
        $review = DB::table('rating_review')
                    ->select('uid As user_id','name','title','review','score','created_at','updated_at')
                    ->where('pid',$id)
                    ->get();
        
        $arr['product'] = $products;
        $arr['review'] = $review;
        
        return json_encode($arr);
    }
    
    public function getOrderById()
    {
        $id = $_REQUEST['id'];
        $arr = array();
        
        $order = DB::table('orders_temp')
                        ->where('id',$id)
                        ->first();
        
        $bill_address_id = $order->bill_address_id;
        $bill_address = '';
        if($bill_address_id != '')
        {
            $bill_address = DB::table('addresses')
                        ->where('address_id',$bill_address_id)
                        ->first();
        }
        
        $ship_address_id = $order->ship_address_id;
        $ship_addres = '';
        if($ship_address_id != '')
        {
            $ship_addres = DB::table('addresses')
                        ->where('address_id',$ship_address_id)
                        ->first();
        }
        
        $uid = $order->uid;
        $user = DB::table('customers_auth')
                    ->select('uid As id','name','email','phone','gender','dob','anniversary','created_at')
                    ->where('uid',$uid)
                    ->first();
        
        $arr['order'] = $order;
        $arr['bill_address'] = $bill_address;
        $arr['ship_address'] = $ship_addres;
        $arr['customer'] = $user;
        return json_encode($arr);
    }
    
    public function getUserProfile()
    {
        $session = $this->getSession();
        $seller_id = $session['seller_id'];
        
        if($seller_id == 0)
        {
            $user = DB::table('dashboard_user')
                        ->where('seller_id',$seller_id)
                        ->get();
        }
        else
        {
            $user = DB::table('dashboard_user')
                    ->where('dashboard_user.seller_id',$seller_id)
                    ->join('jeweller_information', 'jeweller_information.id', '=', 'dashboard_user.seller_id')
                    ->get();

        }
        return json_encode($user);
    }
    
    public function getDashboardStats()
    {
        $session = $this->getSession();
        $seller_id = $session['seller_id'];
        
        if($seller_id == 0)
        {
            $user = DB::table('customers_auth')
                        ->select(DB::raw('count(*) as count'))
                        ->first();
            
            $conf_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','CONFIRMED')
                    ->first();
            
            $order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->first();
            
            $not_conf_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','NOT_CONFIRMED')
                    ->first();
            
            $processing_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','PROCESSING')
                    ->first();
            
            $delivered_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','DELIVERED')
                    ->first();
            
            $cancelled_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','CANCELLED')
                    ->first();
            
            $product = DB::table('products')
                    ->select(DB::raw('count(*) as count'))
                    ->first();
            
            $cart = DB::table('cart')
                    ->select(DB::raw('count(DISTINCT(user_id)) as count'))
                    ->where('is_active',1)
                    ->first();
            
            $review = DB::table('rating_review')
                    ->select(DB::raw('count(*) as count'))
                    ->first();
            
            $wishlist = DB::table('watchlist')
                    ->select(DB::raw('count(DISTINCT(product_id)) as count'))
                    ->first();
        }
        else
        {
            $user = DB::table('customers_auth')
                        ->select(DB::raw('count(*) as count'))
                        ->where('register_through',$seller_id)
                        ->first();
            
            $order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('visited_through',$seller_id)
                    ->first();
            
            $conf_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','CONFIRMED')
                    ->where('visited_through',$seller_id)
                    ->first();
            
            $not_conf_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','NOT_CONFIRMED')
                    ->where('visited_through',$seller_id)
                    ->first();
            
            $processing_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','PROCESSING')
                    ->where('visited_through',$seller_id)
                    ->first();
            
            $delivered_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','DELIVERED')
                    ->where('visited_through',$seller_id)
                    ->first();
            
            $cancelled_order = DB::table('orders_temp')
                    ->select(DB::raw('count(*) as count'))
                    ->where('status','CANCELLED')
                    ->where('visited_through',$seller_id)
                    ->first();
            
            
            $product = DB::table('products')
                    ->select(DB::raw('count(*) as count'))
                    ->where('jeweller_id',$seller_id)
                    ->first();
            
            $cart = DB::table('cart')
                    ->select(DB::raw('count(DISTINCT(user_id)) as count'))
                    ->where('is_active',1)
                    ->first();
            
            $review = DB::table('rating_review')
                    ->select(DB::raw('count(*) as count'))
                    ->first();
            
            $wishlist = DB::table('watchlist')
                    ->select(DB::raw('count(DISTINCT(product_id)) as count'))
                    ->first();
        }
        
        $arr['customer'] = $user;
        $arr['order'] = $order;
        $arr['not_conf_order'] = $not_conf_order;
        $arr['conf_order'] = $conf_order;
        $arr['processing_order'] = $processing_order;
        $arr['delivered_order'] = $delivered_order;
        $arr['cancelled_order'] = $cancelled_order;
        $arr['product'] = $product;
        $arr['cart'] = $cart;
        $arr['review'] = $review;
        $arr['wishlist'] = $wishlist;
        
        return json_encode($arr);
    }
    
    public function generateInvoice(Request $request)
    {
        $id = $request->get('id');
        $filename = public_path()."/invoice/"."invoice-for-order-VIVO".$id.".pdf";
        
        $order = DB::table('orders_temp')
                        ->where('id',$id)
                        ->first();
        
        $order_data['id'] = "VIVO".$order->id;
        $order_data['placed_on'] = date("d-m-Y", strtotime($order->created_at));
        if($order->isCod == "0")
        {
            $order_data['payment_mode'] = "Online";
        }
        else if($order->isCod == "1")
        {
            $order_data['payment_mode'] = "Cash On Delivery";
        }
        $order_data = $this->array_to_object($order_data);
        
        $total = $order->total;
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $words = $f->format($total);
        $words = ucwords(str_replace("-"," ",$words));
        
        $parr = json_decode($order->items, true);
        $index = 0;
        $products = array();
        $grand_total = 0;
        
        foreach ($parr as $p) {
            $products[$index]['title']=$p['item']['title'];
            $products[$index]['quantity']=$p['quantity'];
            $products[$index]['total_amount']=$p['item']['price_after_discount'];
            $products[$index]['tax_rate'] = "3.0%";
            $total_price = round(($p['item']['price_after_discount'] * $p['quantity']) / 1.03);
            $products[$index]['total_price'] = $total_price;
            $products[$index]['unit_price'] = round($total_price / $p['quantity']);
            $products[$index]['total_tax_amount'] = ($p['item']['price_after_discount'] * $p['quantity']) - $total_price;
            
            $grand_total = $grand_total + $p['item']['price_after_discount'];
            $index++;
        }
        $products = $this->array_to_object($products);
        
        $bill_address_id = $order->bill_address_id;
        $bill_address = '';
        if($bill_address_id != '')
        {
            $bill_address = DB::table('addresses')
                        ->where('address_id',$bill_address_id)
                        ->first();
        }
        
        $ship_address_id = $order->ship_address_id;
        $ship_addres = '';
        if($ship_address_id != '')
        {
            $ship_addres = DB::table('addresses')
                        ->where('address_id',$ship_address_id)
                        ->first();
        }
        
        $uid = $order->uid;
        $user = DB::table('customers_auth')
                    ->select('uid As id','name','email','phone','gender','dob','anniversary','created_at')
                    ->where('uid',$uid)
                    ->first();
        
        /*$pdf = PDF::loadView('invoice',compact('order_data','user','order','products','bill_address','ship_addres','words','grand_total'));
        return $pdf->download('invoice.pdf');*/
        
        PDF::loadView('invoice',compact('order_data','user','order','products','bill_address','ship_addres','words','grand_total'))->setPaper('a4')->setWarnings(false)->save($filename);
 
        if (File::exists($filename))
        {
            $response["status"] = "Success";
            $response["filename"] = $filename;
            return json_encode($response);
        }
        else
        {
            $response["status"] = "Error";
            return json_encode($response);
        }
        
                
    }

}
