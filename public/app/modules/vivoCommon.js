Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m" style="width: 300px;">' +
        '<div class="modal-content"style="background-color: rgba(255, 0, 0, 0);box-shadow: none;border: none;">' +

        '<div class="modal-body text-center">' +
        '<svg width="60px" height="60px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="47.5" stroke-dasharray="193.99334635916975 104.45795573186061" stroke="#e62739 " fill="none" stroke-width="5"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>' +
        '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Please wait';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);

function logoutJquery() {
    
    $.get("api/v1/logout", function (data) {

        location.reload();
        FB.init({
            appId: '235303226837165',
            status: true,

            cookie: true,

            xfbml: true
        });

        FB.getLoginStatus(function (response) {
            if (response && response.status === 'connected') {
                FB.logout(function (response) {
                    location.reload(true);
                });
            }
        });

    });
}


var app = angular.module("vivoCommon", ['ui.router', 'ngSanitize', 'ui.select', 'ngAnimate', 'toaster', 'kendo.directives', 'satellizer', 'tagged.directives.infiniteScroll']).constant('API_URL','/api/v1/');

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {


}]);
    
app.run(["$rootScope", "Data", "$location", "$http", "$timeout", "API_URL", function ($rootScope, Data, $location,$http,$timeout,API_URL) {

    $rootScope.login = {};
    $rootScope.customer = {
        name: null,
        password: null
    }
    
    $rootScope.logout = function () {
        Data.get('logout').then(function (results) {
            Data.toast(results);
            $window.location.reload();
        });
    }

    $rootScope.doLogin = function (customer) {
        waitingDialog.show(); 
        if ((customer.name == "" || customer.name == null) && (customer.password == "" || customer.password == null)) {
            waitingDialog.hide(); 
            $rootScope.response = "Username and password is required";
        }
        else if (customer.name == "" || customer.name == null) {
            waitingDialog.hide(); 
            $rootScope.response = "Username is required";
        }
        else if(customer.password == "" || customer.password == null)
        {
            waitingDialog.hide(); 
            $rootScope.response = "Password is required";
        }
        else 
        {      
            $http({
                method: 'POST',
                url : API_URL + 'login',
                params : {
                    customer: JSON.stringify(customer)
                }
            }).then(function successCallback(response){
                waitingDialog.hide(); 
                $rootScope.user = response.data;
                $rootScope.response = response.data.message;
//                alert(response.data.message);
                if (response.data.status == "success") {
                    $rootScope.isAuthenticated = true;
                    $rootScope.goToHome();
                }
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }
    
    $rootScope.logoutJquery = function() {
        $.get("/api/v1/logout", function (data) {
            window.location.href = "/";
        });
    }
    
    Data.get('session').then(function (results) {
            
        if (results.id) {
            $rootScope.authenticated = true;
            $rootScope.id = results.id;
            $rootScope.username = results.username;
            $rootScope.role = results.role;
            $rootScope.seller_id = results.seller_id;
            
            if (window.location.pathname == '/') {
                window.location.href = "/dashboard";
            }

        } else {
            if (window.location.pathname != "/") {
                window.location.href = "/";
            }
        }
    });

}]);

app.directive('vivoHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
//Sticky header 
            $scope.logo = false;

            $(window).bind('scroll', function () {
                //Need to change the height here as well as in ".fixed class" of vivo-header file to make it work //properly
                var navHeight = 50;
                if ($(window).scrollTop() > navHeight) {
                    $('nav').addClass('fixed');
                    $scope.logo = true;
                    $scope.$apply();

                } else {
                    $('nav').removeClass('fixed');
                    $scope.logo = false;
                    $scope.$apply();
                }
            });

            //END of Sticky header

        },
        link: function ($scope, $rootScope, Data, $location) {

        },
        
        templateUrl: 'partials/vivo-header.html'
    };
});

app.directive('vivoFooter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {




        },
        link: function ($scope, $rootScope, Data, $location) {



        },
        templateUrl: 'partials/vivo-footer.html'
    };
});

//Below code is for INR comma system
app.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});