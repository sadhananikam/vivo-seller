var loginapp = angular.module("vivoOrders", ['vivoCommon']);

loginapp.controller("ordersCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getAllOrders'
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = response.data;
        $scope.displaygrid();
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    
    $scope.displaygrid = function(){
        $scope.mainGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Orders.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList,
                //total: $scope.resultList.length,
                schema: {
                    model: {
                        fields: {
                            id: { type: "number" },
                            name: { type: "string" },
                            email: { type: "string" },
                            phone: { type: "string" },
                            status: { type: "string" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 15,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "id",
                title: "Order Id",
                width: 30
            },{
                field: "name",
                title: "Name",
                width: 60
            },{
                field: "email",
                title: "Email",
                width: 50
            },{
                field: "phone",
                title: "Phone",
                width: 30
            }, {
                field: "status",
                title: "Status",
                width: 30
            },{
                field: "created_at",
                title: "Placed On",
                width: 30
            },{ 
                template: "<a href='/order-#:id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
}]);