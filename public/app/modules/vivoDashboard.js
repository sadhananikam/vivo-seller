var loginapp = angular.module("vivoDashboard", ['vivoCommon']);

loginapp.controller("dashboardCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
    
    //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
    //but sometimes it brings up errors with normal resize event handlers
    $.resize.throttleWindow = false;

    var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
    
    //pie chart tooltip example
    var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
    var previousPoint = null;
    
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getDashboardStats'
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = response.data;
        
        var data = [
            { label: "delivered",  data: $scope.resultList.delivered_order.count, color: "#68BC31"},
            { label: "processing",  data: $scope.resultList.processing_order.count, color: "#2091CF"},
            { label: "not confirmed",  data: $scope.resultList.not_conf_order.count, color: "#AF4E96"},
            { label: "cancelled",  data: $scope.resultList.cancelled_order.count, color: "#DA5430"},
            { label: "confirmed",  data: $scope.resultList.conf_order.count, color: "#FEE074"}
          ]
        drawPieChart(placeholder, data);
        
        /**
         we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
         so that's not needed actually.
         */
        placeholder.data('chart', data);
        placeholder.data('draw', drawPieChart);

        placeholder.on('plothover', function (event, pos, item) {
            if(item) {
                if (previousPoint != item.seriesIndex) {
                    previousPoint = item.seriesIndex;
                    var tip = item.series['label'] + " : " + item.series['percent']+'%';
                    $tooltip.show().children(0).text(tip);
                }
                $tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
            } else {
                $tooltip.hide();
                previousPoint = null;
            }

        });
        
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
      
    function drawPieChart(placeholder, data, position) {
          $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    tilt:0.8,
                    highlight: {
                        opacity: 0.25
                    },
                    stroke: {
                        color: '#fff',
                        width: 2
                    },
                    startAngle: 2
                }
            },
            legend: {
                show: true,
                position: position || "ne", 
                labelBoxBorderColor: null,
                margin:[-30,15]
            }
            ,
            grid: {
                hoverable: true,
                clickable: true
            }
         })
     }

}]);