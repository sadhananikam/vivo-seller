var loginapp = angular.module("vivoCustomer", ['vivoCommon']);

loginapp.controller("customerCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
   
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[0];
    urlslug = urlslug.replace('.html','');
    $scope.uid = urlslug.split('-').slice(1);
    
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getCustomerById',
        params : {uid:$scope.uid}
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = '';
        if(response.data !== 'null')
        {
            $scope.resultList = response.data;
            
            if(response.data.order.length > 0)
           {
               $scope.displayordergrid();
           }
            
            if(response.data.cart.length > 0)
           {
               $scope.displaycartgrid();
           }
            
            if(response.data.wishlist.length > 0)
           {
               $scope.displaywishlistgrid();
           }
            
        }
        else
        {
            console.log('error');
        }
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    
    $scope.displayordergrid = function(){
        $scope.mainGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Orders.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList.order,
                //total: $scope.resultList.length,
                schema: {
                    model: {
                        fields: {
                            id: { type: "number" },
                            name: { type: "string" },
                            email: { type: "string" },
                            phone: { type: "string" },
                            status: { type: "string" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 10,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "id",
                title: "Order Id",
                width: 30
            },{
                field: "name",
                title: "Name",
                width: 60
            },{
                field: "email",
                title: "Email",
                width: 50
            },{
                field: "phone",
                title: "Phone",
                width: 30
            }, {
                field: "status",
                title: "Status",
                width: 30
            },{
                field: "created_at",
                title: "Placed On",
                width: 30
            },{ 
                template: "<a href='/order-#:id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
    
    $scope.displaycartgrid = function(){
        $scope.mainCartGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Cart.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList.cart,
                schema: {
                    model: {
                        fields: {
                            product_id: { type: "number" },
                            quantity: { type: "number" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 5,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "product_id",
                title: "Product Id",
                width: 30
            },{
                field: "quantity",
                title: "Quantity",
                width: 60
            },{
                field: "created_at",
                title: "Added On",
                width: 30
            },{ 
                template: "<a href='/product-#:product_id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
    
    $scope.displaywishlistgrid = function(){
        $scope.mainWishlistGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Wishlist.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList.wishlist,
                schema: {
                    model: {
                        fields: {
                            product_id: { type: "number" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 5,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "product_id",
                title: "Product Id",
                width: 30
            },{
                field: "created_at",
                title: "Added On",
                width: 30
            },{ 
                template: "<a href='/product-#:product_id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
    
    function getDateTime() {
        var now     = new Date(); 
        var year    = now.getFullYear();
        var month   = now.getMonth()+1; 
        var day     = now.getDate();
        var hour    = now.getHours();
        var minute  = now.getMinutes();
        var second  = now.getSeconds(); 
        if(month.toString().length == 1) {
            var month = '0'+month;
        }
        if(day.toString().length == 1) {
            var day = '0'+day;
        }   
        if(hour.toString().length == 1) {
            var hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
            var minute = '0'+minute;
        }
        if(second.toString().length == 1) {
            var second = '0'+second;
        }   
        var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;   
         return dateTime;
    }
    
}]);