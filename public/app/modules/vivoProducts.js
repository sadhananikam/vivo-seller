var loginapp = angular.module("vivoProducts", ['vivoCommon']);

loginapp.controller("productsCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getAllProducts'
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = response.data;
        $scope.displaygrid();
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    
    $scope.displaygrid = function(){
        $scope.mainGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Products.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList,
                //total: $scope.resultList.length,
                schema: {
                    model: {
                        fields: {
                            id: { type: "number" },
                            VC_SKU: { type: "string" },
                            price_after_discount: { type: "string" }
                        }
                    }
                },
                pageSize: 5,
            },
            height: 550,
            //groupable: true,
            //sortable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "id",
                title: "Id",
                width: 50
            },
            {
                template: "<div class='product-photo'" +
                                "style='background-image: url(https://www.vivocarat.com/images/products-v2/#: VC_SKU #-1.jpg);'></div>",
                field: "title",
                title: "Image",
                width: 50
            }, {
                field: "VC_SKU",
                title: "VC_SKU",
                width: 80
            }, {
                field: "price_after_discount",
                title: "Price",
                width: 50
            },{ 
                template: "<a href='/product-#:id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
}]);