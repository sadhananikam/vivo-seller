var loginapp = angular.module("vivoUserProfile", ['vivoCommon']);

loginapp.controller("userprofileCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
       
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getUserProfile'
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = '';
        if(response.data.length > 0)
        {
            $scope.resultList = response.data[0];
        }
        else
        {
            console.log('error');
        }
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
}]);