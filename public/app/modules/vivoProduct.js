var loginapp = angular.module("vivoProduct", ['vivoCommon']);

loginapp.controller("productCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
    
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[0];
    urlslug = urlslug.replace('.html','');
    $scope.id = urlslug.split('-').slice(1);
    
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getProductById',
        params : {id:$scope.id}
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = '';
        if(response.data !== 'null')
        {
            $scope.resultList = response.data.product;
            $scope.review = response.data.review;
            if(response.data.review.length > 0)
           {
               $scope.displaygrid();
           }
        }
        else
        {
            console.log('error');
        }
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $scope.displaygrid = function(){
        $scope.mainGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Review.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.review,
                schema: {
                    model: {
                        fields: {
                            id: { type: "number" },
                            name: { type: "string" },
                            email: { type: "string" },
                            phone: { type: "string" },
                            gender: { type: "string" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 15,
            },
            height: 550,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "name",
                title: "Name",
                width: 30
            },{
                field: "title",
                title: "Title",
                width: 60
            },{
                field: "review",
                title: "Review",
                width: 60
            },{
                field: "score",
                title: "Score",
                width: 20
            },{
                field: "created_at",
                title: "Added On",
                width: 30
            },{ 
                template: "<a href='/customer-#:user_id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
    
}]);