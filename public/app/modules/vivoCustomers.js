var loginapp = angular.module("vivoCustomers", ['vivoCommon']);

loginapp.controller("customersCtrl", ["$scope", "$http", "$stateParams", "Data", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, Data, $rootScope, $location, API_URL) {
    
    waitingDialog.show();
    $http({
        method: 'GET',
        url : API_URL + 'getAllCustomers'
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.resultList = response.data;
        $scope.displaygrid();
        waitingDialog.hide();
    },function errorCallback(response){
        console.log(response.data);
    });
    
    
    $scope.displaygrid = function(){
        $scope.mainGridOptions = {
            toolbar: ["excel"],
            excel: {
                fileName: "VivoCarat-Customers.xlsx",
                allPages: true
            },
            dataSource:{
                data : $scope.resultList,
                //total: $scope.resultList.length,
                schema: {
                    model: {
                        fields: {
                            id: { type: "number" },
                            name: { type: "string" },
                            email: { type: "string" },
                            phone: { type: "string" },
                            gender: { type: "string" },
                            created_at: { type: "string" }
                        }
                    }
                },
                pageSize: 15,
            },
            height: 550,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            type: 'aspnetmvc-ajax',
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            columns: [
            {
                field: "id",
                title: "Id",
                width: 20
            },
            {
                field: "name",
                title: "Name",
                width: 50
            },{
                field: "email",
                title: "Email",
                width: 60
            },{
                field: "phone",
                title: "Phone",
                width: 30
            },{
                field: "gender",
                title: "Gender",
                width: 30
            },{
                field: "created_at",
                title: "Registered On",
                width: 30
            },{ 
                template: "<a href='/customer-#:id#.html' target='_self'><i class='menu-icon fa fa-eye'></i></a>", 
                title: "", 
                width: "10px" 
            }

            ]
        }
    }
}]);